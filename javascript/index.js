let choosedArticle = -1;
let draggedArticle = -1;

let normal = true

document.addEventListener('DOMContentLoaded', () => {

  // inventory.addArticle(article = {name : "test" , quantity : 3, price : 2 })
  // inventory.addQuantity("Steak")
  // inventory.removeQuantity("test")
  // inventory.removeArticle("test")

  let inventory = new Inventory()
  let user = new User ("jim", 10)
  let cart = new Cart()
  

  // user.buyArticle(inventory.inventory[1],inventory,cart)
  // user.buyArticle(inventory.inventory[2],inventory,cart)
  // user.buyArticle(inventory.inventory[2],inventory,cart)
  // console.log(cart.total)

  
  // cart.addArticle(article = {name : "test" , price : 2 })
  // cart.removeArticle("Steak")
  
  articleClick()

  // activation en mode drag & drop ou en mode classic 
  const classic = document.getElementById('classic')
  const bonus = document.getElementById('bonus')


    checkMode (classic, bonus,inventory)
    buttonClick(user, inventory, cart)

    attachDragEvents(user,inventory,cart)

})

function checkMode (classic, bonus) {

  
  classic.addEventListener("mousedown", () => {
    normal = true
    classic.classList.add("active")
    classic.classList.add("bg-primary")
    classic.classList.remove("bg-secondary")


    bonus.classList.remove("active")
    bonus.classList.remove("bg-primary")
    bonus.classList.add("bg-secondary")
    



  })

  bonus.addEventListener("mousedown", () => {
    bonus.classList.add("active")
    bonus.classList.add("bg-primary")
    bonus.classList.remove("bg-secondary")


    classic.classList.remove("active")
    classic.classList.remove("bg-primary")
    classic.classList.add("bg-secondary")

    normal = false



  })
}


function articleClick() { // click sur un article
  const articles = document.getElementsByTagName('tr')
  for (let i = 0 ; i< articles.length ; i++) {
      if (articles[i].classList.contains("column")) continue // évite qu'on puisse cliquer sur les titres des column
      articles[i].addEventListener("mousedown", () => {
        if (normal){
          if (articles[i].classList.contains("bg-info")) {
              articles[i].classList.remove("bg-info") // si on clique à nouveau sur un élément il n'est plus en bleu
              document.getElementById("addButton").disabled = true;
              document.getElementById("deleteButton").disabled = true;


              choosedArticle = -1

          }  
          else {
            for (let article2 of articles) {
              article2.classList.remove("bg-info")

            }
            articles[i].classList.add("bg-info") // en suspens, on l'ajoute ?

            if(articles[i].classList.contains("inventory")) { // activer ajouter si on touche à l'inventaire
              document.getElementById("addButton").disabled = false;
              document.getElementById("deleteButton").disabled = true;

            }
            else { // sinon activer suppr
              document.getElementById("deleteButton").disabled = false;
              document.getElementById("addButton").disabled = true;

            }
            choosedArticle = i

          }
        }
    });
    
  }


}


// LES BUTTONS 

function buttonClick(user, inventory, cart){ // click sur un button

  
    const articles = document.getElementsByClassName('inventory')

    document.getElementById("addButton").addEventListener("click", () => { //ajouter au panier
      if (normal){
        if (user.buyArticle(inventory.inventory[choosedArticle-1],inventory,cart)) { 
          articles[choosedArticle-1].classList.add("bg-info")

        }

        // inventory.updateInventory()
        cart.updateCart()
        articleClick()
        // attachDragEvents(user, inventory, cart)

      }
        


    });

    const articlesCart = document.getElementsByClassName('cart') 

    document.getElementById("deleteButton").addEventListener("click", () => {  // retirer du panier
      if (normal){
        if (inventory.addQuantity(cart.inventory[choosedArticle-inventory.inventory.length-2].name, cart.inventory[choosedArticle-inventory.inventory.length-2].price)) {
          cart.removeArticle(cart.inventory[choosedArticle-inventory.inventory.length-2].name)

        } else { // le cas ou l'article n'était plus 
          cart.removeArticle(cart.inventory[choosedArticle-inventory.inventory.length-1].name)


        }
      
        document.getElementById("deleteButton").disabled = true;

        inventory.updateInventory()
        cart.updateCart()
        articleClick()
        // attachDragEvents(user, inventory, cart)

      }


    });
    document.getElementById("payButton").addEventListener("click", () => { 
        if (cart.total === 0) {
          alert("Votre panier est vide ... Vous venez d'acheter du rien pour 0€")

        }else if (user.pay(cart)) {
          alert("Félicitations pour votre achat de " + cart.total + "€, il vous reste " + user.budget + "€")
          cart.total = 0
          cart.clearCart()
          cart.updateTotal()
          cart.updateCart()
        }
        else alert("vous n'avez qu'un budget de " + user.budget + "€ vous ne pouvez pas payer " + cart.total +"€")
      

    });
  
}


// PARTIE BONUS DRAG & DROP

function attachDragEvents(user, inventory, cart) {

  
    const articles = document.getElementsByClassName('inventory')
    const articlesCart = document.getElementsByClassName('cart')

    const drop = document.getElementById("drop") // drop to cart
    const drop2 = document.getElementById("drop2") // drop to inventory



    for (let i = 0 ; i< articles.length ; i++) {
      let mousedown = false
      let mousemove = false
      let offx
      let offy
      articles[i].addEventListener("mousedown", () => {
        if (!normal) {
          offx= articles[i].offsetWidth 
          offy= articles[i].offsetHeight
          draggedArticle = i
          articles[i].classList.add("bg-info")


          mousedown = true;
        }
      });

      document.addEventListener("mousemove", (e) => {
        if (!normal) {
          if (mousedown) {
          
            mousemove = true
            articles[i].classList.add("position-absolute")
            let x 
            let y
            x = (e.clientX - 2*offx/2)
            y = (e.clientY - 2*offy/ 2)
            articles[i].style.top = y + "px" 
            articles[i].style.left = x + "px" 


          }
        }
      });

      drop.addEventListener("mouseup", () => { // quand on lache l'élément
        if (!normal) {
          if (mousedown) {
            user.buyArticle(inventory.inventory[draggedArticle],inventory,cart)


            inventory.updateInventory()
            cart.updateCart()
            attachDragEvents(user, inventory, cart)
            

          }
          mousemove =false
          mousedown = false

          articleClick()
        }

      });


      document.addEventListener("mouseup",() => { 
        if (!normal) {
          if (mousedown) {
            if(mousemove) {
              inventory.updateInventory()
              cart.updateCart()
            }

            attachDragEvents(user, inventory, cart)
            
            

          }
          mousemove =false
          mousedown = false
          articleClick()
        }

      });
    }


    //supprimer

    for (let i = 0 ; i< articlesCart.length ; i++) {
      let mousedown = false
      let mousemove = false
      let offx
      let offy
      articlesCart[i].addEventListener("mousedown", () => {
        if (!normal) {
          offx= articlesCart[i].offsetWidth 
          offy= articlesCart[i].offsetHeight
          draggedArticle = i
          articlesCart[i].classList.add("bg-info")


          mousedown = true;
        }
      });

      document.addEventListener("mousemove", (e) => {
        if (!normal) {
          if (mousedown) {
            mousemove = true
            articlesCart[i].classList.add("position-absolute")
            let x 
            let y
            x = (e.clientX - (12*articlesCart[i].offsetWidth)/2)
            y = (e.clientY - articlesCart[i].offsetHeight/2)
            articlesCart[i].style.top = y + "px" 
            articlesCart[i].style.left = x + "px" 


          }
        }

      });

      drop2.addEventListener("mouseup", () => { // quand on lache l'élément
        if (!normal) {
        
          if (mousedown) {
            
            inventory.addQuantity(cart.inventory[draggedArticle].name, cart.inventory[draggedArticle].price)
            cart.removeArticle(cart.inventory[draggedArticle].name)
        
          

            inventory.updateInventory()
            cart.updateCart()
            attachDragEvents(user, inventory, cart)
            

          }
          mousemove =false
          mousedown = false

          // articleClick()

          // draggedArticle = -1
        }

      });


      document.addEventListener("mouseup",() => { 
        if (!normal) {
          if (mousedown) {
            if(mousemove) {
              inventory.updateInventory()
              cart.updateCart()
            }

            attachDragEvents(user, inventory, cart)
            
            

          }
          mousemove =false
          mousedown = false
          articleClick()
        }

      });
    } 

    

}

