class Cart {

  constructor(){
    let article = {}
    this.inventory = []
    this.updateCart()
    this.total=0
    
  }

  updateCart(){
    let article = {}
    const cartDiv = document.getElementById("panier")
    cartDiv.innerHTML="" // on remet cartDiv à 0 pour pouvoir l'update

    for (article of this.inventory ){
      const newArticle = document.createElement("tr")
      newArticle.classList.add("cart")
      let variable
      for (variable in article) {
        if (variable === "quantity") continue
        const elemArticle = document.createElement("td")
        elemArticle.innerHTML = article[variable]
        if (variable === "price") elemArticle.innerHTML += "€"
        newArticle.append(elemArticle)
        cartDiv.append(newArticle)

      }    
    
    }

  }

  updateTotal(){
    const totalDiv = document.getElementById("total")
    totalDiv.innerHTML = this.total + "€"
  }

  addArticle (article) {
    this.inventory.push(article)
    this.total+=article.price
    this.updateTotal()
    this.updateCart()

  }

  removeArticle(articleName) {
    for (let i = 0; i < this.inventory.length; i++) {
      if(this.inventory[i].name === articleName){
        
        this.total -=  this.inventory[i].price
        this.inventory.splice(i, 1);
        break

      }
    }
    this.updateTotal()
    this.updateCart()

  }

  clearCart(){
    this.inventory = []
  }
}