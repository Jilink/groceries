
class Inventory {
  constructor(){
    let article = {}
    this.inventory = [article = {name : "Salade" , quantity : 3, price : 2 }, article = {name : "Pâtes" , quantity : 2, price : 3 }, article = {name : "Coca-Cola" , quantity : 10, price : 3}, article = {name : "BN" , quantity : 2, price : 5 }, article = {name : "Jambon" , quantity : 4, price : 3 }, article = {name : "Steak" , quantity : 1, price : 6 }]
    this.updateInventory()
    
  }

  updateInventory(){
    let article = {}
    const inventaire = document.getElementById("inventaire")
    inventaire.innerHTML="" // on remet l'inventaire à 0 pour pouvoir l'update

    for (article of this.inventory ){
      const newArticle = document.createElement("tr")
      newArticle.classList.add("inventory")
      let variable
      for (variable in article) {
        const elemArticle = document.createElement("td")
        elemArticle.innerHTML = article[variable]
        if (variable === "price") elemArticle.innerHTML += "€"
        newArticle.append(elemArticle)
        inventaire.append(newArticle)

      }    
    
    }

  }

  addArticle (article) {
    this.inventory.push(article)
    this.updateInventory()

  }

  addQuantity (articleName, articlePrice) {
    for (let i = 0; i < this.inventory.length; i++) {
      if(this.inventory[i].name === articleName){
        this.inventory[i].quantity ++
        this.updateInventory()
        return true
  
        }
    }
    // l'article n'existe pas :
    console.log(articleName,articlePrice)
    let article = {name : articleName , quantity : 1, price : articlePrice }
    this.addArticle(article)
    this.updateInventory()

    return false

  }

  removeQuantity(articleName) {
    for (let i = 0; i < this.inventory.length; i++) {
      if(this.inventory[i].name === articleName){
        this.inventory[i].quantity --
        if(this.inventory[i].quantity < 1) { 
          this.inventory.splice(i, 1)
          document.getElementById("addButton").disabled = true;
          this.updateInventory()
          return false

        }
      }
    }

    this.updateInventory()
    return true


  }

  removeArticle(articleName) {
    for (let i = 0; i < this.inventory.length; i++) {
      if(this.inventory[i].name === articleName){
        this.inventory.splice(i, 1);
      }
    }
    this.updateInventory()

  }

}

// module.exports = 'Inventory' 


