class User {
  
  constructor(name,budget) {
    this.name = name
    this.budget = budget 
  }


  buyArticle (article, inventory, cart) {
      cart.addArticle(article)
      return inventory.removeQuantity(article.name)
    
  }

  pay (cart){
    
    if (this.budget >= cart.total) {

      this.budget-=cart.total
      return true
    }

    return false
  }



}

